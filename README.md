# FoundryVTT starfinder lang fr-FR

EN :  
		This module adds the option to translate in French (FRANCE) language Starfinder System. 
		
		Installation
				In the Add-On Modules option click on Install Module and place the following link in the field Manifest URL
				https://gitlab.com/baktov.sugar/foundryvtt-starfinder-lang-fr-fr/raw/master/starfinder_fr-FR/module.json   
				Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.
FR : 
		Ce module traduit les clés (termes) exposés par le créateur pour ce system de jeu Starfinder. 
		
		Installation
			Dans le menu des modules, cliquer sur "install module" et dans le champs Manifest, copier le lien ci-dessous
			https://gitlab.com/baktov.sugar/foundryvtt-starfinder-lang-fr-fr/raw/master/starfinder_fr-FR/module.json   
			Une fois votre Monde lancé, n'oubliez pas d'activer le module et de définir la langue Française pour ce monde.	